Integrantes: 
Francisco Hauva
Eric Lolli

Requisitos:
NodeJS (version 16) instalado.

Instrucciones:
Instalar dependencias con npm install.
Instalar angular usando el comando: npm install -g @angular/cli

Montar servidor usando el comando: ng serve --open