import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from '../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription, Observable, map } from 'rxjs';
import $ from "jquery";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public show: boolean = false;
  public loginForm: FormGroup;

  private subscription: Subscription = new Subscription();

  constructor(
    private http:HttpClient,
    private fb: FormBuilder,
    private router: Router,
    public _authSrv: AuthService
  ) { 
    this.loginForm = this.fb.group({
      name: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
      repeatPassword: ['', [Validators.required]],
      weight: ['', [Validators.required]]
    })
  }

  ngOnInit(): void {
    this.loginForm.valueChanges.subscribe(() =>{
      
    })
  }

  Register() {
    //this.loadingSrv = true;
    //this.message = null;
    //this.loginForm.disable();
    let credentials = {
      name: this.loginForm.controls['name'].value,
      lastName: this.loginForm.controls['lastName'].value,
      email: this.loginForm.controls['email'].value,
      password: this.loginForm.controls['password'].value,
      weight: this.loginForm.controls['weight'].value
    }
      this.subscription.add(this.authRegister(credentials).subscribe(
      response => {
        console.log(response);
        this.router.navigate(['/login']);
      }
      )
    );
  }

  authRegister(credentials: {
    name: string,
    lastName: string,
    email: string,
    password: string,
    weight: number
  }): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this.http.post<any>("http://localhost:3000/auth/signup", credentials, httpOptions)
      .pipe(map(response => {
        return response;
      })
      );
  }

  doRegister() {
    let credentials = {
      firstName: this.loginForm.controls['name'].value,
      lastName: this.loginForm.controls['lastName'].value,
      email: this.loginForm.controls['email'].value,
      password: this.loginForm.controls['password'].value,
      weight: this.loginForm.controls['weight'].value
    }
    credentials.weight = parseFloat(credentials.weight);
    this.subscription.add(this._authSrv.register(credentials).subscribe(
      response => {
        this.router.navigate(['login']);
      })
    );
  }
}


