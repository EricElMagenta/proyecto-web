import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { HttpClient } from '@angular/common/http';
import { CalendarModule } from 'primeng/calendar';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-edit-alimentos',
  templateUrl: './edit-alimentos.component.html',
  styleUrls: ['./edit-alimentos.component.scss']
})
export class EditAlimentosComponent implements OnInit {

  public loginForm: FormGroup;
  private subscription: Subscription = new Subscription();

  constructor(
    private router: Router,
    private http: HttpClient,
    private fb: FormBuilder,
    public _authSrv: AuthService
  ) {
    this.loginForm = this.fb.group({
      name: ['', [Validators.required]],
      calorieCount: ['', [Validators.required]]
    });
   }

  ngOnInit(): void {
    
  }

  addFood (){
    let info = {
      name: this.loginForm.controls['name'].value,
      calorieCount: this.loginForm.controls['calorieCount'].value
    }
    info.calorieCount = parseFloat(info.calorieCount);
    this.subscription.add(this._authSrv.postFood(info).subscribe())

  }

}
