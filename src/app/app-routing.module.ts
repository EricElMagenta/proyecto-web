import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { MenuAlimentosComponent } from './menu-alimentos/menu-alimentos.component';
import { MenuEjercicioComponent } from './menu-ejercicio/menu-ejercicio.component';
import { MenuPrincipalComponent } from './menu-principal/menu-principal.component';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { RegisterComponent } from './register/register.component';
import { EditAlimentosComponent } from './edit-alimentos/edit-alimentos.component';
import { EditEjerciciosComponent } from './edit-ejercicios/edit-ejercicios.component';
import { AdminGuard } from './shared/guard.guard';


const routes: Routes = [
  { path: '', component: LoginComponent, pathMatch: 'full'},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'menu-principal', component: MenuPrincipalComponent },
  { path: 'menu-alimentos', component: MenuAlimentosComponent, canActivate: [AdminGuard] },
  { path: 'menu-ejercicios', component: MenuEjercicioComponent, canActivate: [AdminGuard] },
  { path: 'perfil-usuario', component: PerfilUsuarioComponent, canActivate: [AdminGuard] },
  { path: 'edit-alimentos', component: EditAlimentosComponent, canActivate: [AdminGuard] },
  { path: 'edit-ejercicios', component: EditEjerciciosComponent, canActivate: [AdminGuard] },
  { path: '**', redirectTo: '/', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
