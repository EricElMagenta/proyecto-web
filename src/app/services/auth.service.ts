import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpHandler, HttpRequest } from '@angular/common/http';

export type User = {
    firstName: string,
    lastName: string,
    email: string
}

export type Food = {
  id: number,
  name: string,
  calorieCount: number,
  createdAt: Date 
}

export type Excercise = {
  id: number,
  name: string,
  calorieCount: number,
  createdAt: Date
}

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  constructor(
    private _httpClient: HttpClient
  ) { }

  register(credentials: {
    firstName: string,
    lastName: string,
    email: string,
    password: string,
  }): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this._httpClient.put<any>("http://localhost:3000/auth/signup", credentials, httpOptions)
      .pipe(map(response => {
        return response;
      })
      );
  }

  getMyUser(){
    return this._httpClient.get<User>("http://localhost:3000/users/me");
  }

  postFood(info:{
    name: string,
    calorieCount: number
  }): Observable<any>{
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this._httpClient.post<any>("http://localhost:3000/food-items/me", info, httpOptions)
    .pipe(map(response => {
      return response;
    }
    ));
  }

  postExcercise(info: {
    name: string,
    calorieCount: number
  }): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };
    return this._httpClient.post<any>("http://localhost:3000/excercise-items/me", info, httpOptions)
      .pipe(map(response => {
        return response;
      }
      ));
  }

  getMyFood(){
    return this._httpClient.get<Array<Food>>("http://localhost:3000/food-items/me");
  }

  getMyExcercice() {
    return this._httpClient.get<Array<Excercise>>("http://localhost:3000/excercise-items/me");
  }

}
