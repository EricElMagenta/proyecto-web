import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    var reqToken: HttpRequest<any>;
    if(localStorage.getItem("token")){
      var obj = JSON.parse(localStorage.getItem('token')!);
      if (obj.access_token){
        reqToken = req.clone({
          headers: req.headers.set('Authorization', `Bearer ${obj.access_token}`)});
      } else {
        reqToken = req.clone();
      }
    } else {
      reqToken = req.clone();
    }
    return next.handle(reqToken).pipe(
      map((event: HttpEvent<any>) => {
        return event;
      })
    );
  }
}
