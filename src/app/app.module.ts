import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';

import { AdminGuard } from './shared/guard.guard';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MenuPrincipalComponent } from './menu-principal/menu-principal.component';
import { MenuEjercicioComponent } from './menu-ejercicio/menu-ejercicio.component';
import { MenuAlimentosComponent } from './menu-alimentos/menu-alimentos.component';
import { ColoresDirective } from './colores.directive';

import { FormGroup, NgForm, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { PerfilUsuarioComponent } from './perfil-usuario/perfil-usuario.component';
import { JwtInterceptor } from './services/jwt.interceptor';
import { EditEjerciciosComponent } from './edit-ejercicios/edit-ejercicios.component';
import { EditAlimentosComponent } from './edit-alimentos/edit-alimentos.component';



@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    MenuPrincipalComponent,
    MenuEjercicioComponent,
    MenuAlimentosComponent,
    ColoresDirective,
    PerfilUsuarioComponent,
    EditEjerciciosComponent,
    EditAlimentosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    AdminGuard,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor, 
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
