import { Component, OnInit } from '@angular/core';
import { FormGroup, NgForm, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import $ from "jquery";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  
  public show: boolean = false;
  public loginForm: FormGroup;


  constructor(
    private router:Router,
    private http:HttpClient,
    private fb: FormBuilder
  ) {
    this.loginForm = this.fb.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  ngOnInit(): void {
   
  }

  login() {
    //this.authService.SignIn(this.loginForm.value['email'], this.loginForm.value['password']);
    let credentials = {
      email: this.loginForm.controls['email'].value,
      password: this.loginForm.controls['password'].value
    };
    console.log(credentials)
    let response = this.http
      .post("http://localhost:3000/auth/signin", credentials)
      .subscribe((response) => {
        this.router.navigate(['/menu-principal']);
        localStorage.setItem("token", JSON.stringify(response));
      });

    
  }

}
