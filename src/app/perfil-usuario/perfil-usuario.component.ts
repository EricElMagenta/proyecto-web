import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { User } from '../services/auth.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-perfil-usuario',
  templateUrl: './perfil-usuario.component.html',
  styleUrls: ['./perfil-usuario.component.scss']
})
export class PerfilUsuarioComponent implements OnInit {

  user:User;

  constructor(
    private http:HttpClient,
    private auth:AuthService
  ) {}

  ngOnInit(): void {
    this.auth
      .getMyUser().subscribe(
        (data: User) => (this.user = data)
      )
  }

}
