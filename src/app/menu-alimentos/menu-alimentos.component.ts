import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, Food } from '../services/auth.service';

@Component({
  selector: 'app-menu-alimentos',
  templateUrl: './menu-alimentos.component.html',
  styleUrls: ['./menu-alimentos.component.scss']
})
export class MenuAlimentosComponent implements OnInit {

  comidas: Food[];

  constructor(
    private auth:AuthService
  ) { }

  ngOnInit(): void {
    this.auth
      .getMyFood().subscribe(
        (data: Array<Food>) => (this.comidas = data)
      )
  }

}
