import { Component, OnInit } from '@angular/core';
import { AuthService, Excercise } from '../services/auth.service';

@Component({
  selector: 'app-menu-ejercicio',
  templateUrl: './menu-ejercicio.component.html',
  styleUrls: ['./menu-ejercicio.component.scss']
})
export class MenuEjercicioComponent implements OnInit {

  ejercicios: Excercise[];

  constructor(
    private auth: AuthService
  ) { }

  ngOnInit(): void {
    this.auth
      .getMyExcercice().subscribe(
        (data: Array<Excercise>) => (this.ejercicios = data)
      )
  }

}
