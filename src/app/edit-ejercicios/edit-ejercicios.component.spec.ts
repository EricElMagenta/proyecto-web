import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditEjerciciosComponent } from './edit-ejercicios.component';

describe('EditEjerciciosComponent', () => {
  let component: EditEjerciciosComponent;
  let fixture: ComponentFixture<EditEjerciciosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditEjerciciosComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditEjerciciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
