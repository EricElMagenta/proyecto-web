import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-edit-ejercicios',
  templateUrl: './edit-ejercicios.component.html',
  styleUrls: ['./edit-ejercicios.component.scss']
})
export class EditEjerciciosComponent implements OnInit {

  public loginForm: FormGroup;
  private subscription: Subscription = new Subscription();

  constructor(
    private router: Router,
    private http: HttpClient,
    private fb: FormBuilder,
    public _authSrv: AuthService
  ) {
    this.loginForm = this.fb.group({
      name: ['', [Validators.required]],
      calorieCount: ['', [Validators.required]]
    });
  }


  ngOnInit(): void {
  }

  addExcercice(){
    let info = {
      name: this.loginForm.controls['name'].value,
      calorieCount: this.loginForm.controls['calorieCount'].value
    }
    info.calorieCount = parseFloat(info.calorieCount);
    this.subscription.add(this._authSrv.postExcercise(info).subscribe())
  }

}
